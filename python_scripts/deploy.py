import sys, getopt
import os
from time import sleep
from requests_toolbelt.multipart.encoder import MultipartEncoder
import requests
from requests.auth import HTTPBasicAuth

def deploy2Cloud( file_path, user, pwd, token ):
   print("This deploys:" + file_path)
   print("authenticate to use splunk rest api")
   
   uri = "https://api.splunk.com/2.0/rest/login/splunk"
   if( not user):
      print("Environment: SPLUNK_USER not set")
      sys.exit(4)
   if( not pwd):
      print("Environment: SPLUNK_PWD not set")
      sys.exit(5)

   # show if gitlab variable masking works
   print("authenticate to use splunk rest api with:", user, " pwd:", pwd, " token:", token)
   
   response = requests.get(uri, auth=HTTPBasicAuth(user, pwd))
   print("response:" + response.content.decode())
   user_token = response.json().get("data").get("token")

   print("get validation from appinspect")

   url = "https://appinspect.splunk.com/v1/app/validate"
   app_name = os.path.basename(file_path)

   fields = {}
   fields.update({"app_package": (app_name, open(file_path, "rb"))})
   fields.update({"included_tags": "private_app"})
   payload = MultipartEncoder(fields=fields)

   headers = {"Authorization": "Bearer {}".format(user_token), "Content-Type": payload.content_type}
   response = requests.request("POST", url, data=payload, headers=headers)

   print(response.status_code)
   print(response.json())
   request_id = response.json().get("request_id")

   print("wait on validation status for request_id " + request_id)
   counter = 0
   while True:
      if (counter > 20): 
         return 10
      
      uri = "https://appinspect.splunk.com/v1/app/validate/status/" + request_id
      headers = {"Authorization": "bearer {}".format(user_token), "Content-Type": payload.content_type}
      response = requests.request("GET", uri, headers=headers)
   
      status = response.json().get("status")
      print(response.json())

      if status == "ERROR":
          print("error on validation status.")
          return 11
      if( status == "SUCCESS"):
          print("success on validation status.")
          break
      sleep(10) # wait another 10 seconds
      counter += 1

   print("deploy with appinspect_token and bearer token")
   #print(response.json())

   url = "https://admin.splunk.com/uneedsecurity-cb/adminconfig/v2/apps/victoria?splunkbase=false"
   #testurl = "http://httpbin.org/post"

   auth = "Bearer " + token
   headers = {"Authorization": auth, "ACS-Legal-Ack":"Y", "X-Splunk-Authorization": "{}".format(user_token),"Content-Type": "application/x-www-form-urlencoded"}
   #print(headers)
   #response = requests.post(testurl, files=files, headers=headers)
   response = requests.post(url, data=open(file_path, 'rb'), headers=headers)
   
   print(response.status_code)
   print(response.json())
   return 0

import splunklib.client as client

def deployOnPremise( file_path, user, pwd, token ):
   print("This deploys:" + file_path)
   print("authenticate to use splunk enterprise rest api")
   
   uri = "https://splunk.bln.uneedsecurity.com"
   if( not user):
      print("Environment: SPLUNK_USER not set")
      sys.exit(4)
   if( not pwd):
      print("Environment: SPLUNK_PWD not set")
      sys.exit(5)

   # show if gitlab variable masking works
   print("authenticate to splunk rest api with:", user, " pwd:", pwd, " token:", token)

   service = client.connect(host="splunk.bln.uneedsecurity.com", port=8089, username=user, password=pwd)
   assert isinstance(service, client.Service)
   print("auth successfull")
   #another approach
   # appcollection = service.apps
   # my_app = appcollection.create('my_app')
   params = {'name':'/opt/splunk/gitlab-apps/rhebo_app.tgz','filename':'true','update':'true'}
   service.post('apps/local/create',**params)
   print("deployment successfull")

   return 0

def main(argv):
   cloudswitch = ''
   inputfile = ''
   user = ''
   pwd = ''
   token = ''
   try:
      opts, args = getopt.getopt(sys.argv[1:],"hi:u:p:t:c:",["help","ifile=","user=","pwd=","token=","cloud"])
   except getopt.GetoptError as e:
      print(e)
      print('deploy.py [-c] -i <inputfile> -u <user> -p <pwd> -t <token>')
      sys.exit(1)
   for opt, arg in opts:
      if opt == '-h':
         print ('Syntax: deploy.py -i <inputfile> -u <user> -p <pwd> -t <token>')
         sys.exit(2)
      elif opt in ("-i", "--ifile"):
         inputfile = arg
         #print("inputfile:", inputfile)
      elif opt in ("-u", "--user"):
         user = arg
         #print("user:", user)
      elif opt in ("-p", "--pwd"):
         pwd = arg
         #print("pwd:", pwd)
      elif opt in ("-t", "--token"):
         token = arg
         #print("token:", token)
      elif opt in ("-c", "--cloud"):
         cloudswitch = arg
         #print("cloudswitch:", cloudswitch)
      if inputfile == "":
         #print ('deploy.py  -i <inputfile>  -u <user> -p <pwd> -t <token>')
         sys.exit(3)
   if cloudswitch == "1":
      deploy2Cloud(inputfile, user, pwd, token )
   else:
      deployOnPremise(inputfile, user, pwd, token )

if __name__ == "__main__":
   main(sys.argv[1:])